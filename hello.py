from flask import Flask, request, render_template
from markupsafe import escape
import json

from numpy import number

user_name = None

def calc_factorial(num: int) -> int:
        if num <= 1:
                return 1
        else:
            return num* calc_factorial(num-1)


def check_json(user,pwd):
    global user_name
    f = open('UserInfo.json')

    data = json.load(f)
    for userData in data["userInfo"]:
        if userData["username"] == user and userData["password"] == pwd:
            user_name = userData["fullname"]
            return True, userData["fullname"]
        else:
            return False, None




app = Flask(__name__)

@app.route("/")
def index():
    return (
        "<h1>Index Page</h1>"
    )

@app.route("/test-number")
def test_number_action():
    global user_name
    number = request.args.get('num', type=int)
    return render_template('factorialPage.html',number = number, factorial = calc_factorial(number),check = True, fullname = user_name)



@app.route("/check-login")
def check_login(user = None, pwd = None):
    user = request.args.get('username')
    pwd = request.args.get('pwd')
    if user == None or pwd == None:
        return render_template('login.html',user = user, pwd = pwd)
    else: 
        check, fullname = check_json(user, pwd)
        return render_template('factorialPage.html',fullname = fullname, check = check)